<?php

/**
 * @todo function_description
 */
function opauth_facebook_admin_settings($form, &$form_state) {
  $form['opauth_facebook_app_id'] = array(
    '#title' => t('App ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('opauth_facebook_app_id'),
    '#required' => TRUE,
  );

  $form['opauth_facebook_app_secret'] = array(
    '#title' => t('App Secret'),
    '#type' => 'textfield',
    '#default_value' => variable_get('opauth_facebook_app_secret'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
