<?php

/**
 * @todo function_description
 */
function opauth_twitter_admin_settings($form, &$form_state) {
  $form['opauth_twitter_consumer_key'] = array(
    '#title' => t('Consumer Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('opauth_twitter_consumer_key'),
    '#required' => TRUE,
  );

  $form['opauth_twitter_consumer_secret'] = array(
    '#title' => t('Consumer Secret'),
    '#type' => 'textfield',
    '#default_value' => variable_get('opauth_twitter_consumer_secret'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
